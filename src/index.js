$(document).ready(function(){

    // AJAX
    $('#myTable').DataTable({
        "ajax": ({
            url: "http://localhost:3000/anagrafica",
            type: "GET",
            dataSrc: "",
            dataType: "json",
        }),

        // COLUMS
        "columns": [
            { "data": "idanagrafica" },
            { "data": "RagioneSociale" },
            { "data": "Indirizzo" },
            { "data": "Citta" },
            { "data": "Prov" },
            { "data": "CAP" },
            { "data": "PIVA" },
            { "data": "CF" },
            { "data": "Telefono" },
            { "data": "Fax" },
            { "data": "Email" },
            {}, {}
        ],

        // COLUMNSDEFS
        "columnDefs": [

            // BUTTON DELETE
            {
                "render": function() {
                    return "<button type='button' class='btn btn-danger'>Delete</button>";
                }, "targets": -2
            },

            // BUTTON EDIT
            {
                "render": function() {
                    return "<button type='button' class='btn btn-success'>Edit</button>";
                }, "targets": -1
            },
        ]
    });

    // FUNCTION DELETE
    $("#myTable").on('click', '.btn-danger', function() {
        $(this).closest('tr').remove();
    });

    // FUNCTION EDIT 
    $('#myTable').on('click','.btn-success',function() {
        let idanagrafica=$(this).parents('tr').find('td:eq(0)').text();
        let regionesociale=$(this).parents('tr').find('td:eq(1)').text();
        let indirizzo=$(this).parents('tr').find('td:eq(2)').text();
        let citta=$(this).parents('tr').find('td:eq(3)').text();
        let prov=$(this).parents('tr').find('td:eq(4)').text();
        let cap=$(this).parents('tr').find('td:eq(5)').text();
        let piva=$(this).parents('tr').find('td:eq(6)').text();
        let cf=$(this).parents('tr').find('td:eq(7)').text();
        let telefono=$(this).parents('tr').find('td:eq(8)').text();
        let fax=$(this).parents('tr').find('td:eq(9)').text();
        let email=$(this).parents('tr').find('td:eq(10)').text();

        $(this).parents('tr').find('td:eq(0)').html("<input class='form-control' name='idanagrafica' value='"+idanagrafica+"'>");
        $(this).parents('tr').find('td:eq(1)').html("<input class='form-control' name='regionesociale' value='"+regionesociale+"'>");
        $(this).parents('tr').find('td:eq(2)').html("<input class='form-control' name='indirizzo' value='"+indirizzo+"'>");
        $(this).parents('tr').find('td:eq(3)').html("<input class='form-control' name='citta' value='"+citta+"'>");
        $(this).parents('tr').find('td:eq(4)').html("<input class='form-control' name='prov' value='"+prov+"'>");
        $(this).parents('tr').find('td:eq(5)').html("<input class='form-control' name='cap' value='"+cap+"'>");
        $(this).parents('tr').find('td:eq(6)').html("<input class='form-control' name='piva' value='"+piva+"'>");
        $(this).parents('tr').find('td:eq(7)').html("<input class='form-control' name='cf' value='"+cf+"'>");
        $(this).parents('tr').find('td:eq(8)').html("<input class='form-control' name='telefono' value='"+telefono+"'>");
        $(this).parents('tr').find('td:eq(9)').html("<input class='form-control' name='fax' value='"+fax+"'>");
        $(this).parents('tr').find('td:eq(10)').html("<input class='form-control' name='email' value='"+email+"'>");

        $(this).parents('tr').find('td:eq(12)').prepend("<button type='button' class='btn btn-info'>Update</button>");
        $(this).hide()
    });

    $('#myTable').on('click','.btn-info',function() {
        let idanagrafica=$(this).parents('tr').find("input[name='idanagrafica']").val();
        let regionesociale=$(this).parents('tr').find("input[name='regionesociale']").val();
        let indirizzo=$(this).parents('tr').find("input[name='indirizzo']").val();
        let citta=$(this).parents('tr').find("input[name='citta']").val();
        let prov=$(this).parents('tr').find("input[name='prov']").val();
        let cap=$(this).parents('tr').find("input[name='cap']").val();
        let piva=$(this).parents('tr').find("input[name='piva']").val();
        let cf=$(this).parents('tr').find("input[name='cf']").val();
        let telefono=$(this).parents('tr').find("input[name='telefono']").val();
        let fax=$(this).parents('tr').find("input[name='fax']").val();
        let email=$(this).parents('tr').find("input[name='email']").val();

        $(this).parents('tr').find('td:eq(0)').text(idanagrafica);
        $(this).parents('tr').find('td:eq(1)').text(regionesociale);
        $(this).parents('tr').find('td:eq(2)').text(indirizzo);
        $(this).parents('tr').find('td:eq(3)').text(citta);
        $(this).parents('tr').find('td:eq(4)').text(prov);
        $(this).parents('tr').find('td:eq(5)').text(cap);
        $(this).parents('tr').find('td:eq(6)').text(piva);
        $(this).parents('tr').find('td:eq(7)').text(cf);
        $(this).parents('tr').find('td:eq(8)').text(telefono);
        $(this).parents('tr').find('td:eq(9)').text(fax);
        $(this).parents('tr').find('td:eq(10)').text(email);

        $(this).parents('tr').attr('data-idangrafica', idanagrafica);
        $(this).parents('tr').attr('data-regionesociale', regionesociale);
        $(this).parents('tr').attr('data-indirizzo', indirizzo);
        $(this).parents('tr').attr('data-citta', citta);
        $(this).parents('tr').attr('data-prov', prov);
        $(this).parents('tr').attr('data-cap', cap);
        $(this).parents('tr').attr('data-piva', piva);
        $(this).parents('tr').attr('data-cf', cf);
        $(this).parents('tr').attr('data-telefono', telefono);
        $(this).parents('tr').attr('data-fax', fax);
        $(this).parents('tr').attr('data-email', email);

        $(this).parents('tr').find('.btn-success').show();
        $(this).parents('tr').find('.btn-info').remove();  
    });
  
    // FUNCTION ADD ROWS
    $("form").submit(function(e) {
        e.preventDefault();
        let idanagrafica=$("input[name='idanagrafica']").val();
        let regionesociale=$("input[name='regionesociale']").val();
        let indirizzo=$("input[name='indirizzo']").val();
        let citta=$("input[name='citta']").val();
        let prov=$("input[name='prov']").val();
        let cap=$("input[name='cap']").val();
        let piva=$("input[name='piva']").val();
        let cf=$("input[name='cf']").val();
        let telefono=$("input[name='telefono']").val();
        let fax=$("input[name='fax']").val();
        let email=$("input[name='email']").val();

        $(".data-table tbody").append("<tr><td>"+idanagrafica+"</td><td>"+regionesociale+"</td><td>"+indirizzo+"</td><td>"
        +citta+"</td><td>"+prov+"</td><td>"+cap+"</td><td>"+piva+"</td><td>"+cf+"</td><td>"+telefono+"</td><td>"+fax+"</td><td>"+email+"</td>"
        +"<td><button type='button' class='btn btn-danger'>Delete</button></td>"
        +"<td><button type='button' class='btn btn-success'>Edit</button></td></tr>");
        
        $("input[name='']").val("");
    });

    // NASCONDI FORM
    $('.form-show').hide();

    $('.btn-primary').click(function() {
        $('.form-show').show();

        $('.btn-primary').hide();
    })
    $('.btn-warning').click(function() {
        $('.form-show').hide();

        $('.btn-primary').show();
    })
        
 });